#!/usr/bin/env python3

import math
import json
from dataclasses import dataclass
import random
import sys

# this script generates a configuration for fordite.
# Each frame will vary slightly from the previous one,
# so they can be put together as an animation

n_frames = 3600


width = 1920
height = 1080
max_layer = 90

@dataclass
class State:
    p1: (float, float, float)
    d1: (float, float, float)
    p2: (float, float, float)
    d2: (float, float, float)
    p_hm: (float, float)
    d_hm: (float, float)

@dataclass
class Box:
    lower: (float, float, float)
    upper: (float, float, float)

def move_point(p: (float, float, float), d: (float, float, float), box: Box) -> ((float, float, float), (float, float, float)):
    (dx, dy, dz) = d
    dx += random.randint(-1, 1)
    dy += random.randint(-1, 1)
    dz += random.randint(-1, 1)
    # if the velocity is too high, it will look ugly. Let's limit it to 20
    dabs = (dx**2 + dy**2 + dz**2)**0.5
    if dabs > 20:
        dx = dx/dabs * 20
        dy = dy/dabs * 20
        dz = dz/dabs * 20
    (px, py, pz) = (p[0] + d[0], p[1] + d[1], p[2] + d[2])
    if px < box.lower[0]:
        px = box.lower[0]
        dx = -dx
    if py < box.lower[1]:
        py = box.lower[1]
        dy = -dy
    if pz < box.lower[2]:
        pz = box.lower[2]
        dz = -dz
    if px > box.upper[0]:
        px = box.upper[0]
        dx = -dx
    if py > box.upper[1]:
        py = box.upper[1]
        dy = -dy
    if pz > box.upper[2]:
        pz = box.upper[2]
        dz = -dz
    return ((px, py, pz), (dx, dy, dz))

BOX1 = Box((-width, -height, 0), (width*2, height*2, height*2))
BOX2 = Box((width/4, height/4, height / 2), (3 * width/4, 3 * height/4, height))

def move(state: State) -> State:
    (p1, d1) = move_point(state.p1, state.d1, BOX1)
    (p2, d2) = move_point(state.p2, state.d2, BOX2)
    (dhm_x, dhm_y) = state.d_hm
    dhm_x += random.randint(-1, 1)
    dhm_y += random.randint(-1, 1)
    dhm_abs = (dhm_x**2 + dhm_y**2)**0.5
    dhm_x = dhm_x / dhm_abs * 10
    dhm_y = dhm_x / dhm_abs * 10
    (hm_x, hm_y) = state.p_hm
    hm_x += dhm_x
    hm_y += dhm_y
    if hm_x < width/4:
        hm_x = width/4
        dhm_x = -dhm_x
    if hm_y < height/4:
        hm_y = height/4
        dhm_y = -dhm_y
    if hm_x > width/4 * 3:
        hm_x = width/4 * 3
        dhm_x = -dhm_x
    if hm_y > height/4 * 3:
        hm_y = height/4 * 3
        dhm_y = -dhm_y
    return State(p1, d1, p2, d2, (hm_x, hm_y), (dhm_x, dhm_y))

def frame(i: int, state: State) -> (dict, State):
    cfg = {
        "width": width,
        "height": height,
        "max_layer": max_layer,
        "output": "frame_%04d.png" % i,
        "height_map": {
            "type": "polar_sine",
            "params": {
                "translation_x": state.p_hm[0],
                "translation_y": state.p_hm[1],
                "period": 200
            }
        },
        "palette": ["ff0000", "880000", "ffff00", "ffff88", "ee8800", "eeaa00", "440000", "ff1100"],
        "layers": {
            "type": "cylinder",
            "params": {
                "origin": state.p1,
                "orientation": [state.p2[0]-state.p1[0], state.p2[1]-state.p1[1], state.p2[2]-state.p1[2]],
                "layer_thickness": 7
            }
        },
        "rand_seed": 9001
    }
    new_state = move(state)
    return (cfg, new_state)

random.seed(9001)
config = []
state = State((width/2, 0, 0), (-10, 10, 0), (width/2, height/2, height/2), (10, -10, -5), (width/2, height/2), (0, 10))
for i in range(n_frames):
    (f, state) = frame(i, state)
    (dx, dy, dz) = (state.p2[0] - state.p1[0], state.p2[1] - state.p1[0], state.p2[2] - state.p1[2])
    l = (dx**2 + dy**2 + dz**2)**0.5
    config.append(f)


print(json.dumps(config))
