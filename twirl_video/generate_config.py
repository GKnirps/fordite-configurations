#!/usr/bin/env python3

import math
import json

# this script generates a configuration for fordite.
# Each frame will vary slightly from the previous one,
# so they can be put together as an animation

n_frames = 120

def frame(i):
    return {
        "width": 1920,
        "height": 1080,
        "max_layer": 256,
        "output": "frame_%04d.png" % i,
        "height_map": {
            "type": "polar_sine",
            "params": {
                "translation_x": 960,
                "translation_y": 540,
                "period": 200
            }
        },
        #"palette": ["de92dc", "b48adf", "8adfdb", "8adfb1", "c6b765"],
        "palette": ["000000", "ff0000", "ffff00", "ffffff", "0000ff", "0087ff"],
        "layers": {
            "type": "barber_pole",
            "params": {
                "origin": [0,0,0],
                "orientation": [1920, 1080, 256],
                "interval": 600,
                "num_slices": 32,
                "offset": 2 * math.pi * i / n_frames
            }
        },
        "rand_seed": 4 # chosen by fair dice roll, guaranteed to be random
    }

config = list(map(frame, range(n_frames)))

print(json.dumps(config))
