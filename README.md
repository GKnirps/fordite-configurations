Fordite-Configurations
======================

These are configuration files and other useful stuff for [fordite](https://gitlab.com/GKnirps/fordite).

Everything in this repository is licensed under [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/).
