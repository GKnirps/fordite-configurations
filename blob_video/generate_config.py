#!/usr/bin/env python3

import math
import json

# this script generates a configuration for fordite.
# Each frame will vary slightly from the previous one,
# so they can be put together as an animation

period_1 = 240
period_2 = 120
n_frames = 240
offset_1_period_frames = 60
offset_2_period_frames = 80

def frame(i):
    return {
        "width": 1920,
        "height": 1080,
        "max_layer": 192,
        "output": "frame_%04d.png" % i,
        "height_map": {
            "type": "sum",
            "params": [
                {
                    "weight": 1,
                    "type": "sine",
                    "params": {
                        "orientation": 0.8,
                        "period": period_1,
                        "phase_offset": (i % offset_1_period_frames) / offset_1_period_frames * 2 * math.pi,
                    }
                },
                {
                    "weight": 2,
                    "type": "sine",
                    "params": {
                        "orientation": 1.6,
                        "period": period_2,
                        "phase_offset": (i % offset_2_period_frames) / offset_2_period_frames * 2 * math.pi,
                    }
                }
            ]
        },
        "palette": ["0000ee", "000000", "000080", "CCCCCC", "800000"],
        "layers": {
            "type": "sphere",
            "params": {
                "center": [960, 540, 0],
                "layer_thickness": 15
            }
        },
        "rand_seed": 4 # chosen by fair dice roll, guaranteed to be random
    }

config = list(map(frame, range(n_frames)))

print(json.dumps(config))
