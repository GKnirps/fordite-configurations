#!/usr/bin/env python3

import math
import json

# this script generates a configuration for fordite.
# Each frame will vary slightly from the previous one,
# so they can be put together as an animation

period = 45
n_frames = period * 5

def frame(i):
    return {
            "width": 1920,
            "height": 1080,
            "max_layer": 256,
            "output": "frame_%04d.png" % i,
            "height_map": {
                "type": "sine",
                "params": {
                    "orientation": 2 * math.pi * i / n_frames,
                    "translation_x": i % period,
                    "period": period
                }
            },
            "palette": ["05804b", "28c864", "46c87a", "83c8a5", "bafccd"],
            "layers": {
                "type": "sphere",
                "params": {
                    "center": [270, 480, 0],
                    "layer_thickness": 15
                }
            },
            "rand_seed": 42
    }

config = list(map(frame, range(n_frames)))

print(json.dumps(config))
